#include <iostream>
#include <string>

using namespace std;

string ReverseString(string val)
{
  string ret;
  ret.resize(val.size());
  const int len = val.length();
  for (int i = 0; i < len; i++)
  {
    ret[i] = val[len - 1 - i];
  }
  return ret;
}

int main(void)
{
  cout << "Please insert a string you want to reverse" << endl;
  string ans;
  getline(cin, ans);
  cout << "Your String: " << ans << endl << "Reversed String: " << ReverseString(ans) << endl;
  system("pause");
  return 0;
}
