#pragma once
#define _USE_MATH_DEFINES
#include <cmath>
#undef _USE_MATH_DEFINES

class MyMath
{
private:
  float Deg2Rad(const float deg) const;

public:
  MyMath();
  ~MyMath();

  float Add(const float a, const float b) const;
  float Sub(const float a, const float b) const;
  float Mul(const float a, const float b) const;
  float Div(const float a, const float b) const;

  float Pow(const float a, const float b) const;
  float Sqrt(const float a) const;

  float Round(const float a) const;

  float Sin(const float deg) const;
  float Cos(const float deg) const;
  float Tan(const float deg) const;

  float Min(const float a, const float b) const;
  float Max(const float a, const float b) const;

  float LogN(const float a) const;
  float Log10(const float a) const;
};