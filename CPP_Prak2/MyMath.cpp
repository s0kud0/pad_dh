#include "MyMath.h"

float MyMath::Deg2Rad(const float deg) const
{
  return deg * M_PI / 180;
}

MyMath::MyMath()
{
}

MyMath::~MyMath()
{
}

float MyMath::Add(const float a, const float b) const
{
  return a + b;
}

float MyMath::Sub(const float a, const float b) const
{
  return a - b;
}

float MyMath::Mul(const float a, const float b) const
{
  return a * b;
}

float MyMath::Div(const float a, const float b) const
{
  return a / b;
}

float MyMath::Pow(const float a, const float b) const
{
  return std::pow(a, b);
}

float MyMath::Sqrt(const float a) const
{
  return std::sqrt(a);
}

float MyMath::Round(const float a) const
{
  return std::round(a);
}

float MyMath::Sin(const float deg) const
{
  return std::sin(Deg2Rad(deg));
}

float MyMath::Cos(const float deg) const
{
  return std::cos(Deg2Rad(deg));
}

float MyMath::Tan(const float deg) const
{
  return std::tan(Deg2Rad(deg));
}

float MyMath::Min(const float a, const float b) const
{
  return std::fmin(a, b);
}

float MyMath::Max(const float a, const float b) const
{
  return std::fmax(a, b);
}

float MyMath::LogN(const float a) const
{
  return std::log(a);
}

float MyMath::Log10(const float a) const
{
  return std::log10(a);
}
