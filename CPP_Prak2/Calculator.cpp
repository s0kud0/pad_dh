#include "Calculator.h"

Calculator::Calculator()
{
}

Calculator::~Calculator()
{
}

float Calculator::Calculate(const std::string task, const float a, const float b) const
{
  if (task == "Add")
  {
    return math.Add(a, b);
  }
  if (task == "Sub")
  {
    return math.Sub(a, b);
  }
  if (task == "Mul")
  {
    return math.Mul(a, b);
  }
  if (task == "Div")
  {
    return math.Div(a, b);
  }
  if (task == "Pow")
  {
    return math.Pow(a, b);
  }
  if (task == "Sqrt")
  {
    return math.Sqrt(a);
  }
  if (task == "Round")
  {
    return math.Round(a);
  }
  if (task == "Sin")
  {
    return math.Sin(a);
  }
  if (task == "Cos")
  {
    return math.Cos(a);
  }
  if (task == "Tan")
  {
    return math.Tan(a);
  }
  if (task == "Min")
  {
    return math.Min(a, b);
  }
  if (task == "Max")
  {
    return math.Max(a, b);
  }
  if (task == "LogN")
  {
    return math.LogN(a);
  }
  if (task == "Log10")
  {
    return math.Log10(a);
  }
  return -1;
}
