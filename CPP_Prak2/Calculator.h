#pragma once
#include "MyMath.h"
#include <string>

class Calculator
{
private:
  MyMath math;
public:
  Calculator();
  ~Calculator();

  float Calculate(const std::string expression, const float a, const float b = 0) const;
};