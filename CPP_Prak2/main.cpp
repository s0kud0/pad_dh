#include "Calculator.h"
#include <iostream>
#include <vector>
#include <locale>

Calculator calc;

size_t StrFind(std::string string, size_t pos, char* values, int vCount)
{
  std::vector<size_t> ret;
  for (int i = 0; i < vCount; i++)
  {
    ret.push_back(string.find(values[i], pos));
  }

  for (size_t i = 1; i < ret.size(); i++)
  {
    if (ret[i] < ret[0])
    {
      ret[0] = ret[i];
    }
  }
  if (ret[0] == string.size() - 1)
    return std::string::npos;
  return ret[0];
}

int main()
{
  for(;;)
  {
    std::cout << "Please insert a task: <task> <val a> <val b = 0>" << std::endl;
    std::string value;
    getline(std::cin, value);

    size_t pos = 0;
    std::vector<std::string> substrings;
    for (;;)
    { // ADD 
      //size_t nPos = value.find(' ', pos);

      size_t nPos = StrFind(value, pos, " (,)", 4);

      substrings.push_back(value.substr(pos, nPos - pos));
      if (nPos == std::string::npos)
        break;
      pos = nPos + 1;
    }
    if (substrings.size() == 1)
    {
      if (substrings[0] == "Exit")
        break;
    }
    if (substrings.size() == 2)
    {
      std::cout << "Your Task: " << substrings[0] << " With Value: " << substrings[1] << std::endl;
      std::cout << "Calculation: " << calc.Calculate(substrings[0], strtof(substrings[1].c_str(), nullptr)) << std::endl;
    }
    else if (substrings.size() == 3)
    {
      std::cout << "Your Task: " << substrings[0] << " With Values: " << substrings[1] << "; " << substrings[2] << std::endl;
      std::cout << "Calculation: " << calc.Calculate(substrings[0], strtof(substrings[1].c_str(), nullptr), strtof(substrings[2].c_str(), nullptr)) << std::endl;
    }
    std::cout << std::endl;
  }
  return 0;
}