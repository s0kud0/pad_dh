#include "include/ZSUmrechner.h"

int main(void)
{
  ZSUmrechner umrechner;
  while(true)
  {
    cout << "Please insert a Base 10 number and a new base" << endl;
    long number, base;
    cin >> number >> base;
    umrechner.SetDezWert(number);
    umrechner.SetBasis(base);
    umrechner.Umrechnen();
    umrechner.Browse();
  }
    return 0;
}
