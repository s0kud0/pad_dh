#pragma once
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class ZSUmrechner
{
protected:
  long dezWert; // Dezimalzahl, die ungerechnet wird
  string ergWert; // oder char ergWert[100]; das Ergebnis als (C-)String
  long basis; // Basis, in die umgerechnet wird

public:
  ZSUmrechner(void);
  ~ZSUmrechner(void);
  void SetBasis(long); // Basis eingeben/schreiben
  long GetBasis(void) const; // Basis ausgeben/lesen
  void SetDezWert(long); // DezWert eingeben/schreiben
  long GetDezWert(void) const; // DezWert ausgeben/lesen
  void Umrechnen(void); // DezWert in ErgWert umrechnen
  void Browse(void) const; // Alle Attribute am Bildschirm ausgeben
};

