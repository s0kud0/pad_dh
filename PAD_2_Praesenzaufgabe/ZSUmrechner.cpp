#include "include/ZSUmrechner.h"

//#define SHOW_OUTPUT


ZSUmrechner::ZSUmrechner(void): dezWert(0), basis(0)
{
}


ZSUmrechner::~ZSUmrechner(void)
{
}

void ZSUmrechner::SetBasis(const long basis)
{
  if (basis <= 1 || basis >= 37)
  {
    cout << "Bitte nur Zahlen zwischen 1 und 37 eingeben" << endl;
  }
  else
  {
    this->basis = basis;
  }
}

long ZSUmrechner::GetBasis(void) const
{
  return this->basis;
}

void ZSUmrechner::SetDezWert(const long dezWert)
{
  this->dezWert = dezWert;
}

long ZSUmrechner::GetDezWert(void) const
{
  return this->dezWert;
}

void ZSUmrechner::Umrechnen(void)
{
  long nWert = this->dezWert;
  vector<char> arr;
  do
  {
#ifdef SHOW_OUTPUT
    cout << "nWert: " << nWert << " mod " << this->basis << " = " << (nWert % this->basis) << endl << "new nWert: " << (nWert / this->basis) << endl;
#endif
    arr.push_back(nWert % this->basis);
    nWert /= this->basis;
  } while (nWert != 0);
  this->ergWert = "";
  for (int i = arr.size() - 1; i >= 0; i--)
  {
    if (arr[i] >= 10)
    {
      this->ergWert.push_back(arr[i] - 10 + 'A');
    }
    else
    {
      this->ergWert.push_back(arr[i] + '0');
    }
  }
  this->ergWert.push_back('\0');
}

void ZSUmrechner::Browse(void) const
{
  cout << "DezWert: " << this->dezWert << " zur Basis 10 " << endl << "Ergebnis: " << this->ergWert << " zur Basis " << this->basis << endl;
}
