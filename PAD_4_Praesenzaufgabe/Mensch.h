#pragma once
#include <iomanip>
#include <iostream>

class Mensch
{
protected:
    char name[80] = {0};
  float vermoegen = 0;

public:
  char* getName(void);
  float getVermoegen(void) const;
  void addVermoegen(const float value);
  void subVermoegen(const float value);

  Mensch(char* name);
  virtual ~Mensch(void);

  virtual void browse(void);
};
