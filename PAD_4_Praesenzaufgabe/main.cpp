//#include "vld.h"

#include "Frau.h"
#include <Thread>
#include <chrono>
#include <iostream>
#include <cmath>

int main(int argc, const char * argv[])
{
  Frau* amalie = new Frau("Amalie K.");
  Mensch* max = new Mensch("Max M.");
  Mensch* kinder[4];
  for (int jahr = 1900; jahr <= 1999; jahr++)
  {
    switch (jahr)
    {
    case 1925:
      amalie->heirat(max);
      amalie->addVermoegen(100000);
      break;
    case 1927:
      kinder[0] = new Mensch("Alfons");
      amalie->kind(1, kinder[0]);
      break;
    case 1929:
      kinder[1] = new Mensch("Mathilde");
      amalie->kind(2, kinder[1]);
      amalie->subVermoegen(100000);
      break;
    case 1935:
      kinder[2] = new Mensch("Sieglinde");
      kinder[3] = new Mensch("Renate");
      amalie->kind(3, kinder[2]);
      amalie->kind(4, kinder[3]);
      break;
    case 1951:
      amalie->stirbtKind(2);
      delete kinder[1];
      kinder[1] = nullptr;
      amalie->addVermoegen(6000);
      break;
    case 1952:
      amalie->stirbtEhemann();
      delete max;
      break;
    case 1972:
      amalie->addVermoegen(12300000);
    default:
      break;
    }
    if (jahr > 1951 && jahr < 1972)
    {
      amalie->subVermoegen(1000);
      if (amalie->getVermoegen() < 0)
      {
          amalie->subVermoegen(std::abs(amalie->getVermoegen()) * 0.08f);
      }
    }
    else if (jahr > 1972)
    {
      amalie->subVermoegen(10000);
      if (amalie->getVermoegen() < 0)
      {
          amalie->subVermoegen(std::abs(amalie->getVermoegen()) * 0.08f);
      }
    }
    std::cout << jahr << ": ";
    amalie->browse();
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
  }
  delete amalie;
  for (size_t i = 0; i < 4; i++)
  {
    if (kinder[i] != nullptr)
    {
      delete kinder[i];
    }
  }
  return 0;
}
