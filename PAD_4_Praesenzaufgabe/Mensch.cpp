#include "Mensch.h"

using namespace std;

Mensch::Mensch(char* name)
{
    strcpy(this->name, name);
}

Mensch::~Mensch(void)
{
    
}

char* Mensch::getName(void)
{
    return this->name;
}

float Mensch::getVermoegen() const
{
  return this->vermoegen;
}

void Mensch::addVermoegen(const float value)
{
  this->vermoegen += value;
}

void Mensch::subVermoegen(const float value)
{
  this->vermoegen -= value;
}

void Mensch::browse(void)
{
    cout << " Mensch " << this->getName() << " mit Vermoegen " << std::fixed << std::setprecision(2) << this->vermoegen << endl;
}
