#include "Frau.h"
using namespace std;

Frau::Frau(char* name) : Mensch(name)
{
}

Frau::~Frau(void)
{

}

void Frau::kind(int n, Mensch* kind)
{
    if (n < 1 || n > 10)
    {
        return;
    }
    this->kinder[n - 1] = kind;
}

void Frau::heirat(Mensch* mann)
{
  this->ehemann = mann;
}

void Frau::stirbtKind(int n)
{
    if (n < 1 || n > 10) return;
    n = n - 1;
    if (this->kinder[n] != nullptr)
    {
        this->kinder[n] = nullptr;
    }
}

void Frau::stirbtEhemann(void)
{
    if (this->ehemann != nullptr)
    {
      this->ehemann = nullptr;
    }
}

void Frau::zeige(void)
{
    cout << "Frau " << this->name << " mit Vermoegen " << std::fixed << std::setprecision(2) << this->vermoegen << " ";
    if (this->ehemann != nullptr)
    {
        cout << " mit Ehemann " << this->ehemann->getName() << " ";
    }
    for (int i = 0; i < 10; i++)
    {
        if (this->kinder[i] != nullptr)
        {
            cout << " mit Kind Nr. " << i + 1 << " Namens " << this->kinder[i]->getName() << " ";
        }
    }
    cout << endl;
}

void Frau::browse(void)
{
    this->zeige();
}
