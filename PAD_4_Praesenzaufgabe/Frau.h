#pragma once
#include "Mensch.h"

class Frau : public Mensch
{
protected:
  Mensch* ehemann = nullptr;
  Mensch* kinder[10] = {0};

public:
  Frau(char* name);
  virtual ~Frau(void);
  void kind(int index, Mensch* kind);
  void heirat(Mensch* name);
  void stirbtKind(int index);
  void stirbtEhemann(void);
  void zeige(void);

  void browse(void) override final;
};
