#include "Anwendungsprogramm.h"

Anwendungsprogramm::Anwendungsprogramm()
{
    auto now = chrono::system_clock::now();
    auto value = chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
    srand(value.count());
}


Anwendungsprogramm::~Anwendungsprogramm()
{
    
}

void Anwendungsprogramm::Ausfuehren()
{
    for(;;)
    {
        int i = rand() % 20;
        if (i < 10)
        {
            Auto& car = this->cars[i];
            car.Browse();
            
            float strecke = rand() % 1000;
            if (strecke / 100 * car.Verbrauch() > car.TankInhalt())
            {
                Tankstelle& tanke = this->gasStations[rand() % 5];
                car.Tankt(100, tanke);
                tanke.Browse();
            }
            car.Faehrt(strecke);
            car.Browse();
        }
        this_thread::sleep_for(chrono::milliseconds(200));
        cout << endl;
    }
}

void Anwendungsprogramm::GenerateCars()
{
	for (size_t i = 0; i < 10; i++)
	{
		string carName = "Auto ";
        carName.append(to_string(i));
		cars.push_back(Auto(carName, 0, 100, (rand() % 400 + 400) / 100.0f));
	}
}

void Anwendungsprogramm::GenerateGasStations()
{
	for (size_t i = 0; i < 5; i++)
	{
		string gasStationName = "Gas Station ";
        gasStationName.append(to_string(i));
		gasStations.push_back(Tankstelle(gasStationName));
	}
}

void Anwendungsprogramm::Browse()
{
}
