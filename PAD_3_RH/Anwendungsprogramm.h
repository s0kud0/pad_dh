#pragma once
#include "Auto.h"
#include "Tankstelle.h"
#include <string>
#include <vector>
#include <thread>
#include <chrono>

class Anwendungsprogramm
{
private:
	vector<Auto> cars;
	vector<Tankstelle> gasStations;
public:
	Anwendungsprogramm();
	~Anwendungsprogramm();

	void Ausfuehren();
	void GenerateCars();
	void GenerateGasStations();
	void Browse();

};

