#pragma once
#include "Tankstelle.h"
#include <iostream>
#include <string>
#include <thread>
#include <chrono>

using namespace std;

class Auto
{
private:
	string name;
	float kmStand;
	float tankInhalt;
	float verbrauch;
	float tankMaximum;

public:
	Auto(string name);
	Auto(string name, float kmStand, float tankInhalt, float verbrauch);
	~Auto(void);

	void Faehrt(float strecke);
	void Tankt(float menge, Tankstelle& tankstelle);
	void Browse(void);
    
    inline float TankInhalt()
    {
        return this->tankInhalt;
    }
    
    inline float Verbrauch()
    {
        return this->verbrauch;
    }
};

