#include "Auto.h"



Auto::Auto(string name)
{
	this->name = name;
	this->kmStand = 0;
	this->tankInhalt = 100;
	this->verbrauch = 5.0f;
	this->tankMaximum = 100;
}

Auto::Auto(string name, float kmStand, float tankInhalt, float verbrauch)
{
	this->name = name;
	this->kmStand = kmStand;
	this->tankInhalt = tankInhalt;
	this->verbrauch = verbrauch;
	this->tankMaximum = 100;
}


Auto::~Auto(void)
{
}

void Auto::Faehrt(float strecke)
{
    for (float i = 0; i < strecke; ++i)
    {
        this->kmStand += 1;
        this->tankInhalt -= 1.0f / 100 * verbrauch;
        this_thread::sleep_for(chrono::nanoseconds(int(10000 * tankInhalt)));
    }
}

void Auto::Tankt(float menge, Tankstelle& tankstelle)
{
	if (tankInhalt + menge > tankMaximum)
	{
		cout << "Tankvorgang nicht moeglich Tank ueberfuellt" << endl << "Tankemenge wird automatisch auf Maximum reduziert!" << endl;
		menge = tankMaximum - tankInhalt;
	}
	tankInhalt += tankstelle.GibtAus(menge);	
}

void Auto::Browse(void)
{
	cout << name << " mit Kilometerstand " << kmStand << " und Tankinhalt " << tankInhalt << " und Verbrauch " << verbrauch << endl;
}
