#include <iostream>
#include "Anwendungsprogramm.h"

int main(void)
{
	Anwendungsprogramm programm;

    programm.GenerateCars();
    programm.GenerateGasStations();
	programm.Ausfuehren();

	system("pause");

	return 0;
}
