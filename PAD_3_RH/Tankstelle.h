#pragma once
#include <iostream>
#include <string>

using namespace std;

class Tankstelle
{
private:
	string name;
	float tankinhalt;

public:
	Tankstelle(string name);
	~Tankstelle(void);

	float GibtAus(float menge);
	void Browse(void);

};

