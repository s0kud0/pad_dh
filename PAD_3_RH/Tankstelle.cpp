#include "Tankstelle.h"



Tankstelle::Tankstelle(string name)
{
	this->name = name;
	this->tankinhalt = 100000;
}


Tankstelle::~Tankstelle(void)
{
}

float Tankstelle::GibtAus(float menge)
{
	this->tankinhalt -= menge;
	return menge;
}

void Tankstelle::Browse(void)
{
	cout << this->name << " mit Tankinhalt " << tankinhalt << endl;
}
