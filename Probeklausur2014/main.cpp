//#include "vld.h"

#include "Person.h"
#include "Mannschaft.h"
#include <string.h>
#include <string>

void SortiereMannschaft(Mannschaft& mannschaft)
{
    std::vector<Person*> newMannschaft(30);
    
    for (int i = 0; i < 30; i++) {
        newMannschaft[i] = mannschaft.GetMannschaft()[i];
    }
    
    struct
    {
        bool operator()(Person* a, Person* b)
        {
            return a->GetNummer() < b->GetNummer();
        }
    } customLess;
    
    std::sort(newMannschaft.begin(), newMannschaft.end(), customLess);
    
    mannschaft.SetMannschaft(newMannschaft.data(), 30);
}

int main(void)
{
    char* datei = "Spacko1, Vorspack1, 1\nSpacko3, Vorspack3, 3\nSpacko4, Vorspack4, 4\nSpacko2, Vorspack2, 2\nSpacko5, Vorspack5, 6\nSpacko6, Vorspack6, 5\nSpacko7, Vorspack7, 8\nSpacko8, Vorspack8, 7\nSpacko9, Vorspack9, 9\nSpacko10, Vorspack10, 10\nSpacko11, Vorspack12, 4\nSpacko12, Vorspack12, 12\nSpacko13, Vorspack13, 13\nSpacko14, Vorspack14, 14\nSpacko15, Vorspack15, 15\nSpacko16, Vorspack16, 16\nSpacko17, Vorspack17, 17\nSpacko18, Vorspack18, 18\nSpacko19, Vorspack19, 19\nSpacko20, Vorspack20, 20\nSpacko21, Vorspack21, 21\nSpacko22, Vorspack22, 22\nSpacko23, Vorspack23, 23\nSpacko24, Vorspack24, 24\nSpacko25, Vorspack25, 25\nSpacko26, Vorspack26, 26\nSpacko27, Vorspack27, 27\nSpacko28, Vorspack28, 28\nSpacko29, Vorspack29, 29\nSpacko30, Vorspack30, 30";
    

    
    
    Person trainer("Dagur", "Sigurdsson", 0);
    Mannschaft mannschaft(&trainer);
    
    std::vector<char*> spieler;
    
    char* pch = strtok(datei, "\n");
    spieler.push_back(pch);
    while(pch != NULL)
    {
        pch = strtok(NULL, "\n");
        spieler.push_back(pch);
    }
    
    for (int i = 0; i < 30; i++)
    {
        mannschaft.SetSpieler(new Person(std::string(strtok(spieler[i], ", ")), std::string(strtok(NULL, ", ")), atoi(strtok(NULL, ", "))), i);
    }
    
    mannschaft.Browse();
    
    SortiereMannschaft(mannschaft);
    
    mannschaft.Browse();
    
    for (int i = 0; i < 30; i++) {
        delete mannschaft.GetSpieler(i);
    }
    
    return 0;
}
