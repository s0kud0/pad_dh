#include "Person.h"

Person::Person()
{
    this->vorname = "Arsch";
    this->nachname = "Loch";
    this->nummer = 1;
}

Person::Person(std::string vorname, std::string nachname, int nummer) : vorname(vorname), nachname(nachname), nummer(nummer)
{
    
}

Person::~Person()
{
    
}

void Person::SetVorname(std::string name)
{
    this->vorname = name;
}

std::string Person::GetVorname()
{
    return this->vorname;
}

void Person::SetNachname(std::string name)
{
    this->nachname = name;
}

std::string Person::GetNachname()
{
    return this->nachname;
}

void Person::SetNummer(int nummer)
{
    this->nummer = nummer;
}

int Person::GetNummer()
{
    return this->nummer;
}

void Person::Browse()
{
    std::cout << "Spieler " << this->nummer << " " << this->vorname << " " << this->nachname << std::endl;
}
