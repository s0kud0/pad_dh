#include "Mannschaft.h"

Mannschaft::Mannschaft(Person* trainer) : trainer(trainer)
{
    
}

Mannschaft::~Mannschaft()
{
    
}

void Mannschaft::SetTrainer(Person* trainer)
{
    this->trainer = trainer;
}

Person* Mannschaft::GetTrainer()
{
    return this->trainer;
}

void Mannschaft::SetMannschaft(Person** array, int length)
{
    if (length != 30)
    {
        return;
    }
    
    memcpy(this->mannschaft, array, 30 * sizeof(Person*));
}

Person** Mannschaft::GetMannschaft()
{
    return this->mannschaft;
}

void Mannschaft::SetSpieler(Person* spieler, int index)
{
    if (index >= 0 && index < 30)
    {
        this->mannschaft[index] = spieler;
    }
    else
    {
        std::cout << "Fehler" << std::endl;
    }
}

Person* Mannschaft::GetSpieler(int index)
{
    return this->mannschaft[index];
}

void Mannschaft::Browse()
{
    std::cout << "Mannschaft mit Trainer ";
    this->trainer->Browse();
    
    std::cout << "Und Spielern";
    
    
    
    
    for (int i = 0; i < 30; i++)
    {
        this->mannschaft[i]->Browse();
    }
}
