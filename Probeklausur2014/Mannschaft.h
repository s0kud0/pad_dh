//
//  Person.h
//  PAD
//
//  Created by David Hagens on 12.01.18.
//
//

#ifndef MANNSCHAFT_H
#define MANNSCHAFT_H

#include "Person.h"
#include <iostream>
#include <vector>

class Mannschaft
{
private:
    Person* trainer;
    Person* mannschaft[30];
    
public:
    Mannschaft(Person* trainer);
    ~Mannschaft();
    
    void SetTrainer(Person* trainer);
    Person* GetTrainer();
    
    void SetMannschaft(Person** array, int length);
    Person** GetMannschaft();
    
    void SetSpieler(Person* spieler, int index);
    Person* GetSpieler(int index);
    
    void Browse();
};

#endif /* Person_h */
