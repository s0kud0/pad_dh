//
//  Person.h
//  PAD
//
//  Created by David Hagens on 12.01.18.
//
//

#ifndef Person_h
#define Person_h

#include <iostream>
#include <string>

class Person
{
private:
    std::string vorname, nachname;
    int nummer;
public:
    Person();
    Person(std::string vorname, std::string nachname, int nummer);
    ~Person();
    
    void SetVorname(std::string name);
    std::string GetVorname();
    
    void SetNachname(std::string name);
    std::string GetNachname();

    void SetNummer(int nummer);
    int GetNummer();

    void Browse();
};


#endif /* Person_h */
