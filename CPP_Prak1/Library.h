#pragma once

#include <string>

struct Book
{
  std::string Name;
  bool isLent;
};

class Library
{
private:
  Book books[10];

public:
  Library(void);
  ~Library(void);

  Book* Lend(int bookAddress);
  void Return(int bookAddress);

  int GetAdress(std::string bookName);
  void GetBookName(int bookAddress);
};